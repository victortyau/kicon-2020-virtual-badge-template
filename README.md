# KiCon 2020 Virtual Badge Template

![Render](RENDER/workshop_table_badge.jpg)

## Virtualizing #badgelife

Since this years KiCon will be a virtual event, there won't be a traditional badge to go hack on, but instead, I have made this simple template for a KiCon 2020 badge, that we all can use as a starting point to create our own badges with exactly the stuff we would like to hack on.

There isn't really any rules, but I think it would be cool if the badge you make can be recognized as being part of this, so maybe keeping the overall shape and logo, but if you want to scale it down and make PCB earrings that light up or try out a new color combination from your favorite PCB manufacturer, by all means go for it!

The shape fits within a 100x100mm square, so having these produced in low quantity should be fairly cheap.

With most of the focus for this years event being on the upcomming KiCad version 6, this project is made using a recent nightly build and would require a fairly recent version to open up without errors or a need for manual file tweaking.

There is only very limited time left, but it should be enough to do a simple design and actually order PCBs and have them for the event, but using this to experiment with different ways to render PCBs in 3D and maybe blending it with the real world would ofcourse also be super cool.

## Please share

If you decide to make something, please share the files and not least images so we can all see all the lovely badges. If sharing on social media, please use the #badgelife tag and please let [me](https://thomasflummer.com/) know, I would lovo to see what you have made!
